const express = require("express");
const bodyParser = require("body-parser");
const pg = require('pg');

const config = {
  user: 'postgres',
  database: 'usuarios',
  password: 'postgres',
  host: 'localhost',
  port: 5432,
  ssl: false,
  idleTimeoutMillis: 30000
}

const client = new pg.Pool(config)

// Modelo
class UsuarioModel {
  constructor() {
    this.todos = [];
  }

  async getLisaUsuarios(){
    const res = await client.query('select * from usuario;')
    return res.rows
  }

  getInfoSystema() {
    const nombre = {
      nameSystem: 'proyectoUsuarios',
      version: 'v1.0.0',
      developer: 'Alejandro Lucion Lima Maldonado',
      email: 'allima69@gmail.com'
    };
    console.log(nombre);
    return nombre;
  }

  async addUsuario(usuario) {
    const query = 'INSERT INTO usuario(nombre, edad) VALUES($1, $2) RETURNING *';
    const values = [usuario.nombre, usuario.edad]
    const res = await client.query(query, values)
    return res;
  }

  async getPromedioEdad() {
    const res = await client.query('select avg(edad) as promedioEdad from usuario;')
    return res.rows[0]
  }
}

// Controlador
class UsuarioController {
  constructor(model) {
    this.model = model;
  }

  async getLisaUsuarios() {
    return await this.model.getLisaUsuarios();
  }

  getInfoSystema() {
    return this.model.getInfoSystema();
  }

  async addUsuario(usuario) {
    await this.model.addUsuario(usuario);
  }

  async getPromedioEdad() {
    return await this.model.getPromedioEdad();
  }
}

// Vistas (Rutas)
const app = express();
const usuarioModel = new UsuarioModel();
const usuarioController = new UsuarioController(usuarioModel);

app.use(bodyParser.json());

app.get("/listUsuarios",async  (req, res) => {
  const response = await usuarioController.getLisaUsuarios()
  res.json(response)
});

app.get("/infoSystema", (req, res) => {
  const response = usuarioController.getInfoSystema()
  res.json(response)
});

// Vistas (Rutas) (continuación)
app.post("/nuevoUsuario", (req, res) => {
  const usuario = req.body;
  console.log(req.body)
  usuarioController.addUsuario(usuario);
  res.sendStatus(200);
});

app.get("/promedioEdad",async  (req, res) => {
  const response = await usuarioController.getPromedioEdad()
  res.json(response)
});

app.listen(3000, () => {
  console.log("Server listening on port 3000");
});
